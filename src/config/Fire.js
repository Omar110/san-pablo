import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

//informacion de la aplicacion copiada desde firebase
const config = {
  apiKey: "AIzaSyCPQuAjuQbHyFUtY-NBOHlCEWhxAS7rP8Y",
  authDomain: "xcovery-ca99c.firebaseapp.com",
  projectId: "xcovery-ca99c",
  storageBucket: "xcovery-ca99c.appspot.com",
  messagingSenderId: "791897249182",
  appId: "1:791897249182:web:b5db29e7e064d50462b28f",
  measurementId: "G-307NMTPJK4"
  };
  const fire= firebase.initializeApp(config);
  export default fire