import React, {Component} from 'react';
import './App.css';
import badak from './Imagenes/badak.png';
import fire from './config/Fire';
import Login from './Componentes/Login';
import Home from './Componentes/Home';
import Footer from './Componentes/Footer';


class App extends Component {
  
  state={
    user:{}
  }
  constructor(props){
    super(props);
  }
  componentDidMount(){
    this.authListener();
  }
  authListener(){
    fire.auth().onAuthStateChanged((user)=>{
      if(user){
        this.setState({user});
      }else{
        this.setState({user:null});
      }
    });
  }
  render(){
    return (
      <div className="App">
          <img className='logo' src={badak} />
        {this.state.user ? (<Home />):(<Login/>)}<Footer/>
      </div>
    );
  }
}

export default App;
