import React, { Component } from 'react'
import fire from '../config/Fire';
import Menu from './Menu';
import './Home.css';
import SubirArchivo from './SubirArchivo';
import {
    BrowserRouter as Router,
    Routes,
    Route
  } from "react-router-dom";
export default class Home extends Component {

    logout(){
        fire.auth().signOut();
    }
    render() {
        return (
            <div>
            <Router>
                <Routes>
                    <Route exact path="/" element={<Menu/>}/>
                    <Route exact path="/home" element={<Home/>}/>   
                    <Route exact path="/home/SubirArchivo" element={<SubirArchivo/>}/>
                </Routes>
            </Router>
                <button onClick={this.logout} className='boton'>Cerrar sesion</button>
                <button className='boton2'>Subir Nuevo Documento</button>
            </div>
        )
    }
}
